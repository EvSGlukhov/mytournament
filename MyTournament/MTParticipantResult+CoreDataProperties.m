//
//  MTParticipantResult+CoreDataProperties.m
//  MyTournament
//
//  Created by Евгений Глухов on 30.03.17.
//  Copyright © 2017 Evgeny Glukhov. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "MTParticipantResult+CoreDataProperties.h"

@implementation MTParticipantResult (CoreDataProperties)

@dynamic cards;
@dynamic scoredGoals;
@dynamic assistents;
@dynamic ballMakers;
@dynamic match;
@dynamic team;

@end
