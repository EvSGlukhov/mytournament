//
//  MTParticipantResult+CoreDataProperties.h
//  MyTournament
//
//  Created by Евгений Глухов on 30.03.17.
//  Copyright © 2017 Evgeny Glukhov. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "MTParticipantResult.h"

NS_ASSUME_NONNULL_BEGIN

@interface MTParticipantResult (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *cards;
@property (nullable, nonatomic, retain) NSNumber *scoredGoals;
@property (nullable, nonatomic, retain) NSSet<MTPlayer *> *assistents;
@property (nullable, nonatomic, retain) NSSet<MTPlayer *> *ballMakers;
@property (nullable, nonatomic, retain) MTMatch *match;
@property (nullable, nonatomic, retain) MTParticipant *team;

@end

@interface MTParticipantResult (CoreDataGeneratedAccessors)

- (void)addAssistentsObject:(MTPlayer *)value;
- (void)removeAssistentsObject:(MTPlayer *)value;
- (void)addAssistents:(NSSet<MTPlayer *> *)values;
- (void)removeAssistents:(NSSet<MTPlayer *> *)values;

- (void)addBallMakersObject:(MTPlayer *)value;
- (void)removeBallMakersObject:(MTPlayer *)value;
- (void)addBallMakers:(NSSet<MTPlayer *> *)values;
- (void)removeBallMakers:(NSSet<MTPlayer *> *)values;

@end

NS_ASSUME_NONNULL_END
