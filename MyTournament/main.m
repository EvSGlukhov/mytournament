//
//  main.m
//  MyTournament
//
//  Created by Евгений Глухов on 19.03.17.
//  Copyright © 2017 Evgeny Glukhov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
