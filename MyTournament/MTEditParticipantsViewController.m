//
//  MTEditParticipantsViewController.m
//  MyTournament
//
//  Created by Евгений Глухов on 27.03.17.
//  Copyright © 2017 Evgeny Glukhov. All rights reserved.
//

#import "MTEditParticipantsViewController.h"
#import "MTCreateParticipantViewController.h"
#import "MTDataManager.h"
#import "MTParticipant.h"
#import "MTAddCell.h"
#import "MTTeamCell.h"

@interface MTEditParticipantsViewController () <AddingTeamsDelegate>

@property (strong, nonatomic) NSMutableArray* participants;
@property (strong, nonatomic) NSMutableArray* participantsNames;

@end

@implementation MTEditParticipantsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.participants = [NSMutableArray array];
    self.participantsNames = [NSMutableArray array]; // Имена участников для проверки участия в турнире.
    
    // Взять имеющиеся команды из кордаты
    // добавить ячейки AddCell и TeamCell, начать делать страницу создания команды
    
    self.participants = [[MTDataManager sharedManager] getAllTeams]; // Может занять время, если команд много.
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSInteger numberOfRows;
    
    if (section == 0) {
        
        numberOfRows = 1;
        
    } else {
        
        numberOfRows = [self.participants count] > 0 ? [self.participants count] : 1;
        
    }
    
    return numberOfRows;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString* addCellIdentifier = @"addCell";
    static NSString* teamCellIdentifier = @"teamCell";
    
    if (indexPath.section == 0) {
        
        MTAddCell* cell = [tableView dequeueReusableCellWithIdentifier:addCellIdentifier];
        
        cell.addLabel.text = @"Добавить участника";
        
        return cell;
        
    } else {
        
        MTTeamCell* cell = [tableView dequeueReusableCellWithIdentifier:teamCellIdentifier];
        
        cell.numberLabel.text = [NSString stringWithFormat:@"%ld", indexPath.row + 1];
        
        if ([self.participants count] > 0) {
            
            MTParticipant* participant = [self.participants objectAtIndex:indexPath.row];
            
            cell.numberLabel.text = [NSString stringWithFormat:@"%ld", indexPath.row + 1];
            cell.nameLabel.text = participant.participantName;
            
            if ([self.participantsNames containsObject:cell.nameLabel.text]) {
                
                cell.statusImageView.image = [UIImage imageNamed:@"checkIcon"];
                
            }
            
        } else {
            
            cell.nameLabel.text = @"Нет участников!";
            
        }
        
        return cell;
        
    }
    
    return nil;
    
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
}

#pragma mark - AddingTeamsDelegate

- (void) newParticipantHasBeenAdded {
    
    self.participants = [[MTDataManager sharedManager] getAllTeams];
    
    [self.tableView reloadData];
    
    // Доделать механизм добавления команд
    // Сохранить команды участницы
    // сделать формирование групп - ручное или жеребьевка если группа + плейофф, пары если плейофф.
    
}

#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"addParticipant"]) {
        
        MTCreateParticipantViewController* vc = (MTCreateParticipantViewController*)segue.destinationViewController;
        
        vc.delegate = self;
        
        NSLog(@"ADD");
        
    }
    
}


@end
