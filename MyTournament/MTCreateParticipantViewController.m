//
//  MTCreateParticipantViewController.m
//  MyTournament
//
//  Created by Евгений Глухов on 29.03.17.
//  Copyright © 2017 Evgeny Glukhov. All rights reserved.
//

#import "MTCreateParticipantViewController.h"
#import "MTDataManager.h"
#import "MTAddCell.h"
#import "MTHeaderCell.h"
#import "MTPlayerCell.h"

@interface MTCreateParticipantViewController ()

@property (strong, nonatomic) NSString* participantsType;
@property (strong, nonatomic) NSMutableArray* players;
@property (strong, nonatomic) NSString* participantName;

@end

@implementation MTCreateParticipantViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // ввести headerCell + playerCell
    // оформить создание команды и занесение в кордату
    
    self.players = [NSMutableArray array];
    
    if ([[[MTDataManager sharedManager] tournamentParameters] count] > 0) {
        // Если есть параметры, значит идет заполнение настроек для турнира. После формирования турнира, нужно будет обнулить данный массив.
        self.participantsType = [[[MTDataManager sharedManager] tournamentParameters] objectForKey:@"participantsType"];
        
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource

- (nullable NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    if (section == 1) {
        
        return @"Состав";
        
    }
    
    return nil;
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (section == 0) {
        
        return 1;
        
    } else {
        
        return [self.players count] > 0 ? [self.players count] + 1 : 1;
        
    }

}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString* headerIdentifier = @"headerCell";
    static NSString* addIdentifier = @"addCell";
    static NSString* playerIdentifier = @"playerCell";
    
    if (indexPath.section == 0) {
        
        MTHeaderCell* cell = [tableView dequeueReusableCellWithIdentifier:headerIdentifier];
        
        cell.participantTypeLabel.text = [self.participantsType isEqualToString:@"teams"] ? @"Команда" : @"Участник";
        
        return cell;
        
    } else if (indexPath.section == 1 && indexPath.row == 0) {
        
        MTAddCell* cell = [tableView dequeueReusableCellWithIdentifier:addIdentifier];
        
        cell.addLabel.text = @"Добавить игрока";
        
        return cell;
        
    } else {
        
        MTPlayerCell* cell = [tableView dequeueReusableCellWithIdentifier:playerIdentifier];
        
        cell.playerNameTextField.text = [self.players objectAtIndex:indexPath.row - 1];
        
        return cell;
        
    }
    
    return nil;
    
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
 
    if (indexPath.section == 1 && indexPath.row == 0) {
        // add Player cell
        
        NSIndexPath* addRowIndexPath = [NSIndexPath indexPathForRow:[self.players count] + 1 inSection:1];
        
        [self.players addObject:@""];
        
        [tableView beginUpdates];
        [tableView insertRowsAtIndexPaths:@[addRowIndexPath] withRowAnimation:UITableViewRowAnimationBottom];
        [tableView endUpdates];
        
        MTPlayerCell* cell = [tableView cellForRowAtIndexPath:addRowIndexPath];
        cell.playerNameTextField.placeholder = @"Имя и Фамилия игрока";
        [cell.playerNameTextField becomeFirstResponder];
        
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section > 0) {
        
        return 60;
        
    } else {
        
        return 140;
        
    }
    
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    
    return YES;
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    NSLog(@"CATCH PARAMETER - %@", textField.text);
    
    UITableViewCell* cell = [self indexPathForCellContainingTextField:textField];
    
    NSIndexPath* selectedIndexPath = [self.tableView indexPathForCell:cell];
    
    NSLog(@"selectedIndexPath - %ld : %ld", selectedIndexPath.section, selectedIndexPath.row);
    
    if (selectedIndexPath.section == 0 && selectedIndexPath.row == 0) {
        
        self.participantName = textField.text;
        
    } else {
        
//        MTPlayerCell* playerCell = (MTPlayerCell*)cell;
//        
//        playerCell.playerNameTextField.text = textField.text;
        
        if (![textField.text isEqualToString:@""]) {
            
            [self.players addObject:textField.text];
            
            NSInteger emptyStringIndex = [self.players indexOfObject:@""];
            
            [self.players removeObjectAtIndex:emptyStringIndex];
            
        }
        
    }
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)addButtonAction:(UIBarButtonItem *)sender {
    
    // Валидация (очистка лишних пробелов)
    self.participantName = [self trimSpacesFromString:self.participantName];
    
    NSMutableArray* trimmedPlayers = [NSMutableArray array];
    
    for (NSString* string in self.players) {
        
        [trimmedPlayers addObject:[self trimSpacesFromString:string]];
        
    }
    
    [self.players removeAllObjects];
    
    self.players = trimmedPlayers;
    
    // Занесли команду в БД
    [[MTDataManager sharedManager] addParticipantWithName:self.participantName type:self.participantsType optionalPlayers:self.players];
    
//    NSLog(@"participantName - %@", self.participantName);
    
//    NSLog(@"Состав - %@", self.players);
    
    [self.delegate newParticipantHasBeenAdded];
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

#pragma mark - helpers

- (NSString*) trimSpacesFromString:(NSString*) string {
    
    return [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
}

- (UITableViewCell*) indexPathForCellContainingTextField:(UITextField*) object {
    
    if ([object isKindOfClass:[UITableViewCell class]]) {
        
        return (UITableViewCell*)object;
        
    }
    
    if (!object.superview) {
        
        return nil;
        
    }
    
    return [self indexPathForCellContainingTextField:object.superview];
    
}

@end
