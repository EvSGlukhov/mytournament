//
//  MTTournament+CoreDataProperties.h
//  MyTournament
//
//  Created by Евгений Глухов on 30.03.17.
//  Copyright © 2017 Evgeny Glukhov. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "MTTournament.h"

NS_ASSUME_NONNULL_BEGIN

@interface MTTournament (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *countOfGroups;
@property (nullable, nonatomic, retain) NSNumber *countOfTeamsInGroup;
@property (nullable, nonatomic, retain) NSNumber *countOfTeamsToPlayoff;
@property (nullable, nonatomic, retain) NSNumber *finalInRoundRobin;
@property (nullable, nonatomic, retain) NSNumber *halftimeDuration;
@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSNumber *participantsCount;
@property (nullable, nonatomic, retain) NSString *participantsType;
@property (nullable, nonatomic, retain) NSNumber *playoffGamesInFinal;
@property (nullable, nonatomic, retain) NSNumber *playoffHomeAwayMatches;
@property (nullable, nonatomic, retain) NSString *playoffMatchesSchema;
@property (nullable, nonatomic, retain) NSString *pointsDistribution;
@property (nullable, nonatomic, retain) NSNumber *roundsCount;
@property (nullable, nonatomic, retain) NSString *toPlayoffPriorities;
@property (nullable, nonatomic, retain) NSString *type;
@property (nullable, nonatomic, retain) NSSet<MTPlayer *> *players;
@property (nullable, nonatomic, retain) NSSet<MTStage *> *stages;
@property (nullable, nonatomic, retain) MTStatistics *statistics;

@end

@interface MTTournament (CoreDataGeneratedAccessors)

- (void)addPlayersObject:(MTPlayer *)value;
- (void)removePlayersObject:(MTPlayer *)value;
- (void)addPlayers:(NSSet<MTPlayer *> *)values;
- (void)removePlayers:(NSSet<MTPlayer *> *)values;

- (void)addStagesObject:(MTStage *)value;
- (void)removeStagesObject:(MTStage *)value;
- (void)addStages:(NSSet<MTStage *> *)values;
- (void)removeStages:(NSSet<MTStage *> *)values;

@end

NS_ASSUME_NONNULL_END
