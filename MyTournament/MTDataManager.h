//
//  MTDataManager.h
//  MyTournament
//
//  Created by Евгений Глухов on 19.03.17.
//  Copyright © 2017 Evgeny Glukhov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"
#import <CoreData/CoreData.h>

@interface MTDataManager : NSObject

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@property (strong, nonatomic) NSMutableDictionary* tournamentParameters;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

+ (MTDataManager*) sharedManager;

- (void) setParametersForTournament:(NSMutableDictionary*) parameters;

// Core Data custom methods
- (void) addNewTournamentWithParameters:(NSDictionary*) parameters andContext:(NSManagedObjectContext*) context;
- (void) addParticipantWithName:(NSString*) participantName type:(NSString*) participantType optionalPlayers:(NSMutableArray*) players;

- (NSMutableArray*) getAllTournaments;
- (NSMutableArray*) getAllTeams;

- (void) clearAllData;
- (void) deleteObject:(id) object inContext:(NSManagedObjectContext*) context;

@end
