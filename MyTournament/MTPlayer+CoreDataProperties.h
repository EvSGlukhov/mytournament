//
//  MTPlayer+CoreDataProperties.h
//  MyTournament
//
//  Created by Евгений Глухов on 30.03.17.
//  Copyright © 2017 Evgeny Glukhov. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "MTPlayer.h"

NS_ASSUME_NONNULL_BEGIN

@interface MTPlayer (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *countOfAssists;
@property (nullable, nonatomic, retain) NSNumber *countOfGoals;
@property (nullable, nonatomic, retain) NSString *playerName;
@property (nullable, nonatomic, retain) NSSet<MTParticipantResult *> *assists;
@property (nullable, nonatomic, retain) NSSet<MTTournament *> *playedTournaments;
@property (nullable, nonatomic, retain) NSSet<MTParticipantResult *> *scoredGoals;
@property (nullable, nonatomic, retain) MTParticipant *team;

@end

@interface MTPlayer (CoreDataGeneratedAccessors)

- (void)addAssistsObject:(MTParticipantResult *)value;
- (void)removeAssistsObject:(MTParticipantResult *)value;
- (void)addAssists:(NSSet<MTParticipantResult *> *)values;
- (void)removeAssists:(NSSet<MTParticipantResult *> *)values;

- (void)addPlayedTournamentsObject:(MTTournament *)value;
- (void)removePlayedTournamentsObject:(MTTournament *)value;
- (void)addPlayedTournaments:(NSSet<MTTournament *> *)values;
- (void)removePlayedTournaments:(NSSet<MTTournament *> *)values;

- (void)addScoredGoalsObject:(MTParticipantResult *)value;
- (void)removeScoredGoalsObject:(MTParticipantResult *)value;
- (void)addScoredGoals:(NSSet<MTParticipantResult *> *)values;
- (void)removeScoredGoals:(NSSet<MTParticipantResult *> *)values;

@end

NS_ASSUME_NONNULL_END
