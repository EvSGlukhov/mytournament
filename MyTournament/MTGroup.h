//
//  MTGroup.h
//  MyTournament
//
//  Created by Евгений Глухов on 25.03.17.
//  Copyright © 2017 Evgeny Glukhov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MTStage.h"

@class MTParticipant;

NS_ASSUME_NONNULL_BEGIN

@interface MTGroup : MTStage

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "MTGroup+CoreDataProperties.h"
