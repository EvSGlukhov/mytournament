//
//  MTCreateTournamentViewController.m
//  MyTournament
//
//  Created by Евгений Глухов on 25.03.17.
//  Copyright © 2017 Evgeny Glukhov. All rights reserved.
//

#import "MTCreateTournamentViewController.h"
#import "MTDataManager.h"
#import "MTTournament.h"

@interface MTCreateTournamentViewController ()

@property (strong, nonatomic) NSMutableArray* participantsCountPickerData;
@property (strong, nonatomic) NSMutableArray* teamsCountInGroupPickerData;
@property (strong, nonatomic) NSMutableArray* halftimeDurationPickerData;
@property (strong, nonatomic) NSMutableArray* oneOrTwoPickerData;
@property (strong, nonatomic) NSMutableArray* teamsCountToPlayoffPickerData;

@property (strong, nonatomic) NSString* participantsType;
@property (assign, nonatomic) NSInteger participantsCount;
@property (assign, nonatomic) NSInteger countOfTeamsInGroup;
@property (assign, nonatomic) NSInteger halftimeDuration;
@property (assign, nonatomic) NSInteger roundsCount;
@property (assign, nonatomic) BOOL finalInLeague;
@property (assign, nonatomic) NSInteger gamesInFinal;
@property (assign, nonatomic) NSInteger teamsCountToPlayoff;
@property (strong, nonatomic) NSString* toPlayoffPriorities;
@property (assign, nonatomic) BOOL homeAwayMatches;
@property (strong, nonatomic) NSString* toPlayoffFormat;

//@property (weak, nonatomic) IBOutlet UITextField *tournamentNameTextField;
//@property (weak, nonatomic) IBOutlet UISegmentedControl *participantsTypeControl;
//@property (weak, nonatomic) IBOutlet UIPickerView *participantsCountPicker;
//@property (weak, nonatomic) IBOutlet UISegmentedControl *formatTypeControl;
//@property (weak, nonatomic) IBOutlet UIPickerView *halftimeDurationPicker;
//@property (weak, nonatomic) IBOutlet UIPickerView *roundsCountPicker;
//@property (weak, nonatomic) IBOutlet UISegmentedControl *finalInLeagueControl;
//@property (weak, nonatomic) IBOutlet UISegmentedControl *homeAwayMatchesControl;
//@property (weak, nonatomic) IBOutlet UIPickerView *gamesInPlayoffPicker;
//@property (weak, nonatomic) IBOutlet UIPickerView *teamsCountInGroupPicker;
//@property (weak, nonatomic) IBOutlet UIPickerView *teamsCountToPlayoffPicker;
//@property (weak, nonatomic) IBOutlet UISegmentedControl *toPlayoffPrioritiesControl;
//@property (weak, nonatomic) IBOutlet UISegmentedControl *toPlayoffFormatControl;

// Ячейки, в зависимости от типа соревнования.
@property (strong, nonatomic) NSMutableArray* roundRobinCells;
@property (strong, nonatomic) NSMutableArray* playoffCells;
@property (strong, nonatomic) NSMutableArray* roundRobinPlayoffCells;

@end

#define MAX_PARTICPANTS_COUNT_IN_TOURNAMENT 512
#define MAX_HALFTIME_DURATION 60

@implementation MTCreateTournamentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // по умолчанию
    [self parametersByDefault];
    
    self.roundRobinCells = [NSMutableArray array];
    self.roundRobinPlayoffCells = [NSMutableArray array];
    self.playoffCells = [NSMutableArray array];
    
    for (UITableViewCell* cell in self.creationCellsCollection) {
        
        NSInteger indexOfCell = [self.creationCellsCollection indexOfObject:cell];
        
        switch (indexOfCell) {
            case 0:
                [self.roundRobinCells addObject:cell];
                [self.roundRobinPlayoffCells addObject:cell];
                [self.playoffCells addObject:cell];
                break;
            case 1:
                [self.roundRobinCells addObject:cell];
                [self.roundRobinPlayoffCells addObject:cell];
                [self.playoffCells addObject:cell];
                break;
            case 2:
                [self.roundRobinCells addObject:cell];
                [self.roundRobinPlayoffCells addObject:cell];
                [self.playoffCells addObject:cell];
                break;
            case 3:
                [self.roundRobinCells addObject:cell];
                [self.roundRobinPlayoffCells addObject:cell];
                [self.playoffCells addObject:cell];
                break;
            case 4:
                [self.roundRobinCells addObject:cell];
                [self.roundRobinPlayoffCells addObject:cell];
                [self.playoffCells addObject:cell];
                break;
            case 5:
                [self.roundRobinCells addObject:cell];
                [self.roundRobinPlayoffCells addObject:cell];
                break;
            case 6:
                [self.roundRobinCells addObject:cell];
                break;
            case 7:
                [self.roundRobinPlayoffCells addObject:cell];
                [self.playoffCells addObject:cell];
                break;
            case 8:
                [self.roundRobinCells addObject:cell];
                [self.roundRobinPlayoffCells addObject:cell];
                [self.playoffCells addObject:cell];
                break;
            case 9:
                [self.roundRobinPlayoffCells addObject:cell];
                break;
            case 10:
                [self.roundRobinPlayoffCells addObject:cell];
                break;
            case 11:
                [self.roundRobinCells addObject:cell];
                [self.roundRobinPlayoffCells addObject:cell];
                break;
            case 12:
                [self.roundRobinPlayoffCells addObject:cell];
                break;
                
            default:
                NSLog(@"Unexpected switch case");
                break;
        }
        
    }
    
    self.participantsCountPickerData = [NSMutableArray array];
    
    for (int i = 1; i <= MAX_PARTICPANTS_COUNT_IN_TOURNAMENT; i++) {
        
        [self.participantsCountPickerData addObject:[NSString stringWithFormat:@"%d", i]];
        
    }
    
    self.halftimeDurationPickerData = [NSMutableArray array];
    
    for (int i = 1; i <= MAX_HALFTIME_DURATION; i++) {
        
        [self.halftimeDurationPickerData addObject:[NSString stringWithFormat:@"%d", i]];
        
    }
    
    self.oneOrTwoPickerData = [NSMutableArray array];
    
    for (int i = 1; i <= 2; i++) {
        
        [self.oneOrTwoPickerData addObject:[NSString stringWithFormat:@"%d", i]];
        
    }
    
    self.teamsCountInGroupPickerData = [NSMutableArray array];
    self.teamsCountToPlayoffPickerData = [NSMutableArray array];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - custom methods

- (void) parametersByDefault {
//    @property (strong, nonatomic) NSString* participantsType;
//    @property (assign, nonatomic) NSInteger participantsCount;
//    @property (assign, nonatomic) NSInteger countOfTeamsInGroup;
//    @property (assign, nonatomic) NSInteger halftimeDuration;
//    @property (assign, nonatomic) NSInteger roundsCount;
//    @property (assign, nonatomic) BOOL finalInLeague;
//    @property (assign, nonatomic) NSInteger gamesInFinal;
//    @property (assign, nonatomic) NSInteger teamsCountToPlayoff;
//    @property (strong, nonatomic) NSString* toPlayoffPriorities;
//    @property (assign, nonatomic) BOOL homeAwayMatches;
//    @property (strong, nonatomic) NSString* toPlayoffFormat;
    
    self.participantsType = @"teams";
    self.finalInLeague = YES;
    self.homeAwayMatches = YES;
    self.toPlayoffPriorities = @"personalMatches";
    self.gamesInFinal = 1;
    self.toPlayoffFormat = @"tossUp";
    
}

#pragma mark - UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}


- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    
    if ([pickerView isEqual:self.participantsCountPicker] || ([pickerView isEqual:self.teamsCountInGroupPicker] && self.participantsCount == 0)) {
        
        return [self.participantsCountPickerData count];
        
    } else if ([pickerView isEqual:self.halftimeDurationPicker]) {
        
        return [self.halftimeDurationPickerData count];
        
    } else if ([pickerView isEqual:self.roundsCountPicker] || [pickerView isEqual:self.gamesInPlayoffPicker]) {
        
        return [self.oneOrTwoPickerData count];
        
    } else if ([pickerView isEqual:self.teamsCountToPlayoffPicker] && self.countOfTeamsInGroup != 0) {
        
        return [self.teamsCountToPlayoffPickerData count];
    
    } else if ([pickerView isEqual:self.teamsCountInGroupPicker] && self.participantsCount != 0) {
        
        return [self.teamsCountInGroupPickerData count];
        
    }
    
    return 2;
    
}

#pragma mark - UIPickerViewDelegate

- (nullable NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    if ([pickerView isEqual:self.participantsCountPicker] || [pickerView isEqual:self.teamsCountInGroupPicker]) {
        
        return [self.participantsCountPickerData objectAtIndex:row];
        
    } else if ([pickerView isEqual:self.halftimeDurationPicker]) {
        
        return [self.halftimeDurationPickerData objectAtIndex:row];
        
    } else if ([pickerView isEqual:self.roundsCountPicker] || [pickerView isEqual:self.gamesInPlayoffPicker]) {
        
        return [self.oneOrTwoPickerData objectAtIndex:row];
        
    } else if ([pickerView isEqual:self.teamsCountToPlayoffPicker] && self.countOfTeamsInGroup != 0) {
        
        return [self.teamsCountToPlayoffPickerData objectAtIndex:row];
        
    } else if ([pickerView isEqual:self.teamsCountInGroupPicker] && self.participantsCount != 0) {
        
        return [self.teamsCountInGroupPickerData objectAtIndex:row];
        
    }
    
    return @"no group";
    
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    if ([pickerView isEqual:self.teamsCountInGroupPicker]) {
        
        self.countOfTeamsInGroup = row + 1;
        [self.teamsCountToPlayoffPickerData removeAllObjects];
        for (int i = 1; i <= self.countOfTeamsInGroup; i++) {
            
            [self.teamsCountToPlayoffPickerData addObject:[NSString stringWithFormat:@"%d", i]];
            
        }
        
        [self.teamsCountToPlayoffPicker reloadAllComponents];
        
    } else if ([pickerView isEqual:self.participantsCountPicker]) {
        
        self.participantsCount = row + 1;
        [self.teamsCountInGroupPickerData removeAllObjects];
        [self.teamsCountToPlayoffPickerData removeAllObjects];
        for (int i = 1; i <= self.participantsCount; i++) {
            
            [self.teamsCountInGroupPickerData addObject:[NSString stringWithFormat:@"%d", i]];
            
        }
        [self.teamsCountInGroupPicker reloadAllComponents];
        [self.teamsCountToPlayoffPicker reloadAllComponents];
        
    } else if ([pickerView isEqual:self.halftimeDurationPicker]) {
        self.halftimeDuration = row + 1;
    } else if ([pickerView isEqual:self.roundsCountPicker]) {
        self.roundsCount = row + 1;
    } else if ([pickerView isEqual:self.gamesInPlayoffPicker]) {
        self.gamesInFinal = row + 1;
    } else if ([pickerView isEqual:self.teamsCountToPlayoffPicker]) {
        self.teamsCountToPlayoff = row + 1;
    }
    
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [self.tournamentNameTextField resignFirstResponder];
    
    return YES;
    
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (self.formatTypeControl.selectedSegmentIndex == 0) {
        // Лига
        return [self.roundRobinCells count];
        
    } else if (self.formatTypeControl.selectedSegmentIndex == 1) {
        // на выбывание
        return [self.playoffCells count];
        
    } else if (self.formatTypeControl.selectedSegmentIndex == 2) {
        // группа + плейофф
        return [self.roundRobinPlayoffCells count];
        
    }
    
    return 0;
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell* cell;
    
    if (self.formatTypeControl.selectedSegmentIndex == 0) {
        // Лига
        cell = [self.roundRobinCells objectAtIndex:indexPath.row];
        
    } else if (self.formatTypeControl.selectedSegmentIndex == 1) {
        // на выбывание
        cell = [self.playoffCells objectAtIndex:indexPath.row];
        
    } else if (self.formatTypeControl.selectedSegmentIndex == 2) {
        // группа + плейофф
        cell = [self.roundRobinPlayoffCells objectAtIndex:indexPath.row];
        
    }
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return NO;
    
}

#pragma mark - Actions

- (IBAction)tournamentNameTextFieldAction:(UITextField *)sender {
}

- (IBAction)participantsTypeControlAction:(UISegmentedControl *)sender {
    
    // team, athlete
    if (self.participantsTypeControl.selectedSegmentIndex == 0) {
        self.participantsType = @"team";
    } else {
        self.participantsType = @"athlete";
    }
    
}

- (IBAction)formatTypeControlAction:(UISegmentedControl *)sender {
    
    [self.tableView reloadData];
    
}
- (IBAction)finalInLeagueControl:(UISegmentedControl *)sender {
    
    if (sender.selectedSegmentIndex == 0) {
        
        self.finalInLeague = YES;
        
    } else {
        
        self.finalInLeague = NO;
        
    }
    
}

- (IBAction)homeAwayMatchesControlAction:(UISegmentedControl *)sender {
    
    if (sender.selectedSegmentIndex == 0) {
        
        self.homeAwayMatches = YES;
        
    } else {
        
        self.homeAwayMatches = NO;
        
    }
    
}

- (IBAction)toPlayoffPrioritiesControlAction:(UISegmentedControl *)sender {
    
    if (sender.selectedSegmentIndex == 0) {
        
        self.toPlayoffPriorities = @"personalMatches";
        
    } else {
        
        self.toPlayoffPriorities = @"goalDifference";
        
    }
    
}

- (IBAction)toPlayoffFormatControlAction:(UISegmentedControl *)sender {
    
    if (sender.selectedSegmentIndex == 0) {
        
        self.toPlayoffFormat = @"tossUp";
        
    } else {
        
        self.toPlayoffFormat = @"cross";
        
    }
    
}

- (IBAction)readyButtonPressed:(UIBarButtonItem *)sender {
    
    NSLog(@"ACTION of Ready button");
    
}

- (void) prepareTournamentParameters {
    
    NSLog(@"READY");
    
    // Нужен метод валидации данных!
    
    NSMutableDictionary* tournamentParameters = [NSMutableDictionary dictionary];
    //    @property (nullable, nonatomic, retain) NSNumber *roundsCount;
    //    @property (nullable, nonatomic, retain) NSString *participantsType;
    //    @property (nullable, nonatomic, retain) NSString *type;
    //    @property (nullable, nonatomic, retain) NSNumber *participantsCount;
    //    @property (nullable, nonatomic, retain) NSString *name;
    //    @property (nullable, nonatomic, retain) NSNumber *finalInRoundRobin;
    //    @property (nullable, nonatomic, retain) NSNumber *halftimeDuration;
    //    @property (nullable, nonatomic, retain) NSNumber *playoffHomeAwayMatches;
    //    @property (nullable, nonatomic, retain) NSNumber *playoffGamesInFinal;
    //    @property (nullable, nonatomic, retain) NSNumber *countOfTeamsInGroup;
    //    @property (nullable, nonatomic, retain) NSNumber *countOfTeamsToPlayoff;
    //    @property (nullable, nonatomic, retain) NSNumber *countOfGroups;
    //    @property (nullable, nonatomic, retain) NSString *pointsDistribution;
    //    @property (nullable, nonatomic, retain) NSString *toPlayoffPriorities;
    //    @property (nullable, nonatomic, retain) NSString *playoffMatchesSchema;
    //    @property (nullable, nonatomic, retain) NSSet<MTStage *> *stages;
    //    @property (nullable, nonatomic, retain) MTStatistics *statistics;
    //    @property (nullable, nonatomic, retain) NSSet<MTPlayer *> *players;
    
    // установка параметров для турнира
    
    // required
    
    [tournamentParameters setObject:self.tournamentNameTextField.text forKey:@"name"];
    
    [tournamentParameters setObject:self.participantsType forKey:@"participantsType"];
    
    [tournamentParameters setObject:[NSNumber numberWithInteger:self.participantsCount] forKey:@"participantsCount"];
    
    NSString* tournamentType; // roundRobin, playoff, roundRobinPlayoff
    if (self.formatTypeControl.selectedSegmentIndex == 0) {
        tournamentType = @"roundRobin";
        [tournamentParameters setObject:[NSNumber numberWithInteger:self.roundsCount] forKey:@"roundsCount"];
        [tournamentParameters setObject:[NSNumber numberWithBool:self.finalInLeague] forKey:@"finalInRoundRobin"];
        [tournamentParameters setObject:[NSNumber numberWithInteger:self.gamesInFinal] forKey:@"playoffGamesInFinal"];
        [tournamentParameters setObject:self.toPlayoffPriorities forKey:@"toPlayoffPriorities"];
    } else if (self.formatTypeControl.selectedSegmentIndex == 1) {
        tournamentType = @"playoff";
        [tournamentParameters setObject:[NSNumber numberWithInteger:self.gamesInFinal] forKey:@"playoffGamesInFinal"];
        [tournamentParameters setObject:[NSNumber numberWithBool:self.homeAwayMatches] forKey:@"playoffHomeAwayMatches"];
    } else {
        tournamentType = @"roundRobinPlayoff";
        [tournamentParameters setObject:[NSNumber numberWithInteger:self.roundsCount] forKey:@"roundsCount"];
        [tournamentParameters setObject:[NSNumber numberWithBool:self.homeAwayMatches] forKey:@"playoffHomeAwayMatches"];
        [tournamentParameters setObject:[NSNumber numberWithInteger:self.gamesInFinal] forKey:@"playoffGamesInFinal"];
        [tournamentParameters setObject:[NSNumber numberWithInteger:self.countOfTeamsInGroup] forKey:@"countOfTeamsInGroup"];
        [tournamentParameters setObject:[NSNumber numberWithInteger:self.teamsCountToPlayoff] forKey:@"countOfTeamsToPlayoff"];
        [tournamentParameters setObject:self.toPlayoffPriorities forKey:@"toPlayoffPriorities"];
        [tournamentParameters setObject:self.toPlayoffFormat forKey:@"playoffMatchesSchema"];
        // playoffMatchesSchema - tossUp, cross (жеребьевка или перекрестный выход)
    }
    
    [tournamentParameters setObject:tournamentType forKey:@"type"];
    
    [tournamentParameters setObject:[NSNumber numberWithInteger:self.halftimeDuration] forKey:@"halftimeDuration"];
    
    [[MTDataManager sharedManager] setParametersForTournament:tournamentParameters];
    
    NSMutableDictionary* retrieveDictionary = [[MTDataManager sharedManager] tournamentParameters];
    
    NSLog(@"dictionary - %@", retrieveDictionary);
    
}

#pragma mark - Segue

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"MTEditParticipantsViewController"]) {
        
        [self prepareTournamentParameters];
        
    }
    
}

@end
