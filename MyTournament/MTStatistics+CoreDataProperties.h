//
//  MTStatistics+CoreDataProperties.h
//  MyTournament
//
//  Created by Евгений Глухов on 30.03.17.
//  Copyright © 2017 Evgeny Glukhov. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "MTStatistics.h"

NS_ASSUME_NONNULL_BEGIN

@interface MTStatistics (CoreDataProperties)

@property (nullable, nonatomic, retain) NSSet<MTPlayer *> *topAssistents;
@property (nullable, nonatomic, retain) NSSet<MTPlayer *> *topScorers;
@property (nullable, nonatomic, retain) MTTournament *tournament;

@end

@interface MTStatistics (CoreDataGeneratedAccessors)

- (void)addTopAssistentsObject:(MTPlayer *)value;
- (void)removeTopAssistentsObject:(MTPlayer *)value;
- (void)addTopAssistents:(NSSet<MTPlayer *> *)values;
- (void)removeTopAssistents:(NSSet<MTPlayer *> *)values;

- (void)addTopScorersObject:(MTPlayer *)value;
- (void)removeTopScorersObject:(MTPlayer *)value;
- (void)addTopScorers:(NSSet<MTPlayer *> *)values;
- (void)removeTopScorers:(NSSet<MTPlayer *> *)values;

@end

NS_ASSUME_NONNULL_END
