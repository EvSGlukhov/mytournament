//
//  MTPlayerCell.h
//  MyTournament
//
//  Created by Евгений Глухов on 29.03.17.
//  Copyright © 2017 Evgeny Glukhov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MTPlayerCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UITextField *playerNameTextField;

@end
