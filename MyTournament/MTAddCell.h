//
//  MTAddCell.h
//  MyTournament
//
//  Created by Евгений Глухов on 28.03.17.
//  Copyright © 2017 Evgeny Glukhov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MTAddCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *addLabel;

@end
