//
//  MTMatchWeek+CoreDataProperties.h
//  MyTournament
//
//  Created by Евгений Глухов on 30.03.17.
//  Copyright © 2017 Evgeny Glukhov. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "MTMatchWeek.h"

NS_ASSUME_NONNULL_BEGIN

@interface MTMatchWeek (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *matchweekNumber;
@property (nullable, nonatomic, retain) NSSet<MTMatch *> *matches;
@property (nullable, nonatomic, retain) MTStage *stage;

@end

@interface MTMatchWeek (CoreDataGeneratedAccessors)

- (void)addMatchesObject:(MTMatch *)value;
- (void)removeMatchesObject:(MTMatch *)value;
- (void)addMatches:(NSSet<MTMatch *> *)values;
- (void)removeMatches:(NSSet<MTMatch *> *)values;

@end

NS_ASSUME_NONNULL_END
