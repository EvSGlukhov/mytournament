//
//  MTTournamentCell.h
//  MyTournament
//
//  Created by Евгений Глухов on 19.03.17.
//  Copyright © 2017 Evgeny Glukhov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MTTournamentCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *tournamentNameLabel;

@end
