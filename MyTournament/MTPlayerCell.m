//
//  MTPlayerCell.m
//  MyTournament
//
//  Created by Евгений Глухов on 29.03.17.
//  Copyright © 2017 Evgeny Glukhov. All rights reserved.
//

#import "MTPlayerCell.h"

@implementation MTPlayerCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
