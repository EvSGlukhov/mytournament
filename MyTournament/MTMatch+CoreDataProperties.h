//
//  MTMatch+CoreDataProperties.h
//  MyTournament
//
//  Created by Евгений Глухов on 30.03.17.
//  Copyright © 2017 Evgeny Glukhov. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "MTMatch.h"

NS_ASSUME_NONNULL_BEGIN

@interface MTMatch (CoreDataProperties)

@property (nullable, nonatomic, retain) NSSet<MTParticipantResult *> *matchResults;
@property (nullable, nonatomic, retain) NSSet<MTMatchWeek *> *matchweek;

@end

@interface MTMatch (CoreDataGeneratedAccessors)

- (void)addMatchResultsObject:(MTParticipantResult *)value;
- (void)removeMatchResultsObject:(MTParticipantResult *)value;
- (void)addMatchResults:(NSSet<MTParticipantResult *> *)values;
- (void)removeMatchResults:(NSSet<MTParticipantResult *> *)values;

- (void)addMatchweekObject:(MTMatchWeek *)value;
- (void)removeMatchweekObject:(MTMatchWeek *)value;
- (void)addMatchweek:(NSSet<MTMatchWeek *> *)values;
- (void)removeMatchweek:(NSSet<MTMatchWeek *> *)values;

@end

NS_ASSUME_NONNULL_END
