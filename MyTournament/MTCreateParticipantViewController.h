//
//  MTCreateParticipantViewController.h
//  MyTournament
//
//  Created by Евгений Глухов on 29.03.17.
//  Copyright © 2017 Evgeny Glukhov. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AddingTeamsDelegate <NSObject>

- (void) newParticipantHasBeenAdded;

@end

@interface MTCreateParticipantViewController : UITableViewController

@property (weak, nonatomic) IBOutlet UIBarButtonItem *addButton;
@property (weak, nonatomic) id <AddingTeamsDelegate> delegate;

- (IBAction)addButtonAction:(UIBarButtonItem *)sender;


@end
