//
//  MTParticipant.h
//  MyTournament
//
//  Created by Евгений Глухов on 25.03.17.
//  Copyright © 2017 Evgeny Glukhov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MTGroup.h"

@class MTParticipantResult, MTPlayer;

NS_ASSUME_NONNULL_BEGIN

@interface MTParticipant : MTGroup

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "MTParticipant+CoreDataProperties.h"
