//
//  MTTournamentsViewController.h
//  MyTournament
//
//  Created by Евгений Глухов on 19.03.17.
//  Copyright © 2017 Evgeny Glukhov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface MTTournamentsViewController : UITableViewController <NSFetchedResultsControllerDelegate>

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *addTournamentButton;



- (IBAction)addTournamentAction:(UIBarButtonItem *)sender;

@end
