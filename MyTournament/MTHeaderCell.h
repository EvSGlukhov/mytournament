//
//  MTHeaderCell.h
//  MyTournament
//
//  Created by Евгений Глухов on 29.03.17.
//  Copyright © 2017 Evgeny Glukhov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MTHeaderCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UITextField *participantTypeTextField;
@property (weak, nonatomic) IBOutlet UILabel *participantTypeLabel;

@end
