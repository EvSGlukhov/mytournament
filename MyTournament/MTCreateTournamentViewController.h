//
//  MTCreateTournamentViewController.h
//  MyTournament
//
//  Created by Евгений Глухов on 25.03.17.
//  Copyright © 2017 Evgeny Glukhov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MTCreateTournamentViewController : UITableViewController <UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate>

@property (strong, nonatomic) IBOutletCollection(UITableViewCell) NSArray *creationCellsCollection;

// 13 ячеек в creationCellsCollection. Нужно раскидать проперти и экшены оставшихся, далее начать заполнять методы.


@property (weak, nonatomic) IBOutlet UITextField *tournamentNameTextField;
@property (weak, nonatomic) IBOutlet UISegmentedControl *participantsTypeControl;
@property (weak, nonatomic) IBOutlet UIPickerView *participantsCountPicker;
@property (weak, nonatomic) IBOutlet UISegmentedControl *formatTypeControl;
@property (weak, nonatomic) IBOutlet UIPickerView *halftimeDurationPicker;
@property (weak, nonatomic) IBOutlet UIPickerView *roundsCountPicker;
@property (weak, nonatomic) IBOutlet UISegmentedControl *finalInLeagueControl;
@property (weak, nonatomic) IBOutlet UISegmentedControl *homeAwayMatchesControl;
@property (weak, nonatomic) IBOutlet UIPickerView *gamesInPlayoffPicker;
@property (weak, nonatomic) IBOutlet UIPickerView *teamsCountInGroupPicker;
@property (weak, nonatomic) IBOutlet UIPickerView *teamsCountToPlayoffPicker;
@property (weak, nonatomic) IBOutlet UISegmentedControl *toPlayoffPrioritiesControl;
@property (weak, nonatomic) IBOutlet UISegmentedControl *toPlayoffFormatControl;



- (IBAction)tournamentNameTextFieldAction:(UITextField *)sender;
- (IBAction)participantsTypeControlAction:(UISegmentedControl *)sender;
- (IBAction)formatTypeControlAction:(UISegmentedControl *)sender;
- (IBAction)finalInLeagueControl:(UISegmentedControl *)sender;
- (IBAction)homeAwayMatchesControlAction:(UISegmentedControl *)sender;
- (IBAction)toPlayoffPrioritiesControlAction:(UISegmentedControl *)sender;
- (IBAction)toPlayoffFormatControlAction:(UISegmentedControl *)sender;
- (IBAction)readyButtonPressed:(UIBarButtonItem *)sender;



@end
