//
//  MTStatistics.h
//  MyTournament
//
//  Created by Евгений Глухов on 25.03.17.
//  Copyright © 2017 Evgeny Glukhov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class MTPlayer, MTTournament;

NS_ASSUME_NONNULL_BEGIN

@interface MTStatistics : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "MTStatistics+CoreDataProperties.h"
