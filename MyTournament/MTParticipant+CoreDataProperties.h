//
//  MTParticipant+CoreDataProperties.h
//  MyTournament
//
//  Created by Евгений Глухов on 30.03.17.
//  Copyright © 2017 Evgeny Glukhov. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "MTParticipant.h"

NS_ASSUME_NONNULL_BEGIN

@interface MTParticipant (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *participantName;
@property (nullable, nonatomic, retain) NSString *participantType;
@property (nullable, nonatomic, retain) NSSet<MTGroup *> *group;
@property (nullable, nonatomic, retain) NSSet<MTPlayer *> *lineup;
@property (nullable, nonatomic, retain) NSSet<MTParticipantResult *> *results;

@end

@interface MTParticipant (CoreDataGeneratedAccessors)

- (void)addGroupObject:(MTGroup *)value;
- (void)removeGroupObject:(MTGroup *)value;
- (void)addGroup:(NSSet<MTGroup *> *)values;
- (void)removeGroup:(NSSet<MTGroup *> *)values;

- (void)addLineupObject:(MTPlayer *)value;
- (void)removeLineupObject:(MTPlayer *)value;
- (void)addLineup:(NSSet<MTPlayer *> *)values;
- (void)removeLineup:(NSSet<MTPlayer *> *)values;

- (void)addResultsObject:(MTParticipantResult *)value;
- (void)removeResultsObject:(MTParticipantResult *)value;
- (void)addResults:(NSSet<MTParticipantResult *> *)values;
- (void)removeResults:(NSSet<MTParticipantResult *> *)values;

@end

NS_ASSUME_NONNULL_END
