//
//  AppDelegate.h
//  MyTournament
//
//  Created by Евгений Глухов on 19.03.17.
//  Copyright © 2017 Evgeny Glukhov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

