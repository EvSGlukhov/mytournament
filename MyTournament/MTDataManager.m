//
//  MTDataManager.m
//  MyTournament
//
//  Created by Евгений Глухов on 19.03.17.
//  Copyright © 2017 Evgeny Glukhov. All rights reserved.
//

#import "MTDataManager.h"
#import "MTTournament.h"
#import "MTParticipant.h"
#import "MTPlayer.h"

@implementation MTDataManager

+ (MTDataManager*) sharedManager {
    
    static MTDataManager* manager = nil;
    
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        
        manager = [[MTDataManager alloc] init];
        
        
    });
    
    return manager;
    
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.tournamentParameters = [NSMutableDictionary dictionary];
    }
    return self;
}

- (void) setParametersForTournament:(NSMutableDictionary*) parameters {
    
    self.tournamentParameters = parameters;
    
}

#pragma mark - custom setters

- (void) addNewTournamentWithParameters:(NSDictionary*) parameters andContext:(NSManagedObjectContext*) context {
    
//    @property (nullable, nonatomic, retain) NSNumber *roundsCount;
//    @property (nullable, nonatomic, retain) NSString *participantsType;
//    @property (nullable, nonatomic, retain) NSString *type;
//    @property (nullable, nonatomic, retain) NSNumber *participantsCount;
//    @property (nullable, nonatomic, retain) NSString *name;
//    @property (nullable, nonatomic, retain) NSNumber *finalInRoundRobin;
//    @property (nullable, nonatomic, retain) NSNumber *halftimeDuration;
//    @property (nullable, nonatomic, retain) NSNumber *playoffHomeAwayMatches;
//    @property (nullable, nonatomic, retain) NSNumber *playoffGamesInFinal;
//    @property (nullable, nonatomic, retain) NSNumber *countOfTeamsInGroup;
//    @property (nullable, nonatomic, retain) NSNumber *countOfTeamsToPlayoff;
//    @property (nullable, nonatomic, retain) NSNumber *countOfGroups;
//    @property (nullable, nonatomic, retain) NSString *pointsDistribution;
//    @property (nullable, nonatomic, retain) NSString *toPlayoffPriorities;
//    @property (nullable, nonatomic, retain) NSString *playoffMatchesSchema;
//    @property (nullable, nonatomic, retain) NSSet<MTStage *> *stages;
//    @property (nullable, nonatomic, retain) MTStatistics *statistics;
//    @property (nullable, nonatomic, retain) NSSet<MTPlayer *> *players;
    
    if (!context) {
        
        context = self.managedObjectContext;
        
    }
    
    MTTournament* tournament = [NSEntityDescription insertNewObjectForEntityForName:@"MTTournament" inManagedObjectContext:context];
    
//    tournament.name = name;
    
    [tournament.managedObjectContext save:nil];
    
}

- (void) addParticipantWithName:(NSString*) participantName type:(NSString*) participantType optionalPlayers:(NSMutableArray*) players {
    
    // MTParticipant
//    @property (nullable, nonatomic, retain) NSString *participantType;
//    @property (nullable, nonatomic, retain) NSString *participantName;
//    @property (nullable, nonatomic, retain) NSSet<MTPlayer *> *lineup;
//    @property (nullable, nonatomic, retain) NSSet<MTParticipantResult *> *results;
//    @property (nullable, nonatomic, retain) NSSet<MTGroup *> *group;
    
    // MTPlayer
//    @property (nullable, nonatomic, retain) NSString *playerName;
//    @property (nullable, nonatomic, retain) NSNumber *countOfGoals;
//    @property (nullable, nonatomic, retain) NSNumber *countOfAssists;
//    @property (nullable, nonatomic, retain) MTParticipant *team;
//    @property (nullable, nonatomic, retain) NSSet<MTParticipantResult *> *scoredGoals;
//    @property (nullable, nonatomic, retain) NSSet<MTParticipantResult *> *assists;
//    @property (nullable, nonatomic, retain) NSSet<MTTournament *> *playedTournaments;
    
    MTParticipant* participant = [NSEntityDescription insertNewObjectForEntityForName:@"MTParticipant" inManagedObjectContext:self.managedObjectContext];
    
    participant.participantType = participantType;
    participant.participantName = participantName;
    
    if (players) {
        
        for (NSString* playerName in players) {
            
            MTPlayer* player = [NSEntityDescription insertNewObjectForEntityForName:@"MTPlayer" inManagedObjectContext:self.managedObjectContext];
            
            player.playerName = playerName;
            
            player.team = participant;
            
//            [player.managedObjectContext save:nil];
            
        }
        
    }
    
    [participant.managedObjectContext save:nil];
    
}

#pragma mark - custom getters

- (NSMutableDictionary*) getTournamentParameters {
    
    return self.tournamentParameters;
    
}

- (NSMutableArray*) getAllTournaments {
    
    NSFetchRequest* request = [[NSFetchRequest alloc] init];
    
    NSEntityDescription* entity = [NSEntityDescription entityForName:@"MTTournament" inManagedObjectContext:self.managedObjectContext];
    
    [request setEntity:entity];
    
    NSArray* tournaments = [self.managedObjectContext executeFetchRequest:request error:nil];
    
    return [tournaments mutableCopy];
    
}

- (NSMutableArray*) getAllTeams {
    
    NSFetchRequest* request = [[NSFetchRequest alloc] init];
    
    NSEntityDescription* entity = [NSEntityDescription entityForName:@"MTParticipant" inManagedObjectContext:self.managedObjectContext];
    
    [request setEntity:entity];
    
    NSArray* participants = [self.managedObjectContext executeFetchRequest:request error:nil];
    
    return [participants mutableCopy];
    
}

- (void) deleteObject:(id) object inContext:(NSManagedObjectContext*) context {
    
    if (!context) {
        
        context = self.managedObjectContext;
        
    }
    
    [context deleteObject:object];
    
    [context save:nil];
    
//    NSString* className = NSStringFromClass([object class]);
    
//    NSFetchRequest* request = [[NSFetchRequest alloc] init];
//    
//    NSEntityDescription* entity = [NSEntityDescription entityForName:className inManagedObjectContext:context];
//    
//    [request setEntity:entity];
//    
//    NSArray* tournaments = [self.managedObjectContext executeFetchRequest:request error:nil];
//    
//    for (MTTournament* tournament in tournaments) {
//        
//        [self.managedObjectContext deleteObject:tournament];
//        
//        [self.managedObjectContext save:nil];
//        
//    }
    
}

- (void) clearAllData {
    
    NSFetchRequest* request = [[NSFetchRequest alloc] init];
    
    NSEntityDescription* entity = [NSEntityDescription entityForName:@"MTTournament" inManagedObjectContext:self.managedObjectContext];
    
    [request setEntity:entity];
    
    NSArray* tournaments = [self.managedObjectContext executeFetchRequest:request error:nil];
    
    for (MTTournament* tournament in tournaments) {
        
        [self.managedObjectContext deleteObject:tournament];
        
        [self.managedObjectContext save:nil];
        
    }
    
    NSFetchRequest* request2 = [[NSFetchRequest alloc] init];
    
    NSEntityDescription* entity2 = [NSEntityDescription entityForName:@"MTParticipant" inManagedObjectContext:self.managedObjectContext];
    
    [request2 setEntity:entity2];
    
    NSArray* participants = [self.managedObjectContext executeFetchRequest:request2 error:nil];
    
    for (MTParticipant* participant in participants) {
        
        [self.managedObjectContext deleteObject:participant];
        
        [self.managedObjectContext save:nil];
        
    }
    
}

#pragma mark - Core Data stack

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory {
    // The directory the application uses to store the Core Data store file. This code uses a directory named "EG.MyTournament" in the application's documents directory.
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel {
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"MyTournament" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it.
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    // Create the coordinator and store
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"MyTournament.sqlite"];
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        
        // Пересоздаем файл базы данных, если были изменены сущности!!!
        
        //        Если мы по ходу работы захотим добавить attribute или убрать из Entities, то нам при запуске выдаст ошибку, т.к. изначально созданный файл под данное приложение (база данных) не будет соответствовать измененному. Нужно будет пересоздать файл базы данных, что мы и делаем с помощью следующих двух строк!
        
        [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil];
        
        [_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error];
        
        // Report any error we got.
//        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
//        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
//        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
//        dict[NSUnderlyingErrorKey] = error;
//        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
//        // Replace this with code to handle the error appropriately.
//        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
//        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
//        abort();
    }
    
    return _persistentStoreCoordinator;
}


- (NSManagedObjectContext *)managedObjectContext {
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        NSError *error = nil;
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

@end
