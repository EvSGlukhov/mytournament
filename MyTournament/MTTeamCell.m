//
//  MTTeamCell.m
//  MyTournament
//
//  Created by Евгений Глухов on 28.03.17.
//  Copyright © 2017 Evgeny Glukhov. All rights reserved.
//

#import "MTTeamCell.h"

@implementation MTTeamCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
