//
//  MTGroup+CoreDataProperties.h
//  MyTournament
//
//  Created by Евгений Глухов on 30.03.17.
//  Copyright © 2017 Evgeny Glukhov. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "MTGroup.h"

NS_ASSUME_NONNULL_BEGIN

@interface MTGroup (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *groupName;
@property (nullable, nonatomic, retain) MTStage *stage;
@property (nullable, nonatomic, retain) NSSet<MTParticipant *> *teams;

@end

@interface MTGroup (CoreDataGeneratedAccessors)

- (void)addTeamsObject:(MTParticipant *)value;
- (void)removeTeamsObject:(MTParticipant *)value;
- (void)addTeams:(NSSet<MTParticipant *> *)values;
- (void)removeTeams:(NSSet<MTParticipant *> *)values;

@end

NS_ASSUME_NONNULL_END
