//
//  MTTournamentsViewController.m
//  MyTournament
//
//  Created by Евгений Глухов on 19.03.17.
//  Copyright © 2017 Evgeny Glukhov. All rights reserved.
//

#import "MTTournamentsViewController.h"
#import "MTDataManager.h"
#import "MTTournamentCell.h"

@interface MTTournamentsViewController ()

@property (strong, nonatomic) NSMutableArray* tournaments;

@end

@implementation MTTournamentsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tournaments = [NSMutableArray array];
    
    self.tournaments = [[MTDataManager sharedManager] getAllTournaments];
//    [[MTDataManager sharedManager] clearAllData];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions 

- (IBAction)addTournamentAction:(UIBarButtonItem *)sender {
    
//    UIAlertController* ac = [UIAlertController alertControllerWithTitle:@"Новый турнир" message:@"Введите название турнира" preferredStyle:UIAlertControllerStyleAlert];
//    
//    __block NSString* tournamentName;
//    
//    [ac addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
//        
////        tournamentName = textField.text;
//        
//    }];
//    
//    UIAlertAction* okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//        
//        UITextField* tournamentNameTextField = [[ac textFields] firstObject];
//        
//        tournamentName = tournamentNameTextField.text;
//        
//        [[MTDataManager sharedManager] addNewTournamentWithName:tournamentName andContext:nil];
//        
//        self.tournaments = [[MTDataManager sharedManager] getAllTournaments];
//        
//        [self.tableView reloadData];
//        
//    }];
//    
//    [ac addAction:okAction];
//    
//    [self presentViewController:ac animated:YES completion:nil];
    
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        //remove the deleted object from your data source.
        //If your data source is an NSMutableArray, do this
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            // удаляем из базы
            [[MTDataManager sharedManager] deleteObject:[self.tournaments objectAtIndex:indexPath.row] inContext:nil];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                // как только обновили из базы, обновляем UI
                NSLog(@"class - %@", NSStringFromClass([self.tournaments class]));
                [self.tournaments removeObjectAtIndex:indexPath.row];
                
                [self.tableView beginUpdates];
                [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationBottom];
                [self.tableView endUpdates];
                
            });
            
        });
        
    }
    
    // доделать схемы core data и делать экраны дальше
}

#pragma mark - UITableViewDataSource

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (section == 0) {
        
        return 1;
        
    } else {
        
        return self.tournaments.count;
        
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString* identifier = @"MTTournamentCell";
    
    if (indexPath.section != 0) {
        
        MTTournamentCell* cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        
        NSLog(@"array - %@", self.tournaments);
        NSLog(@"indexPath - %ld %ld", indexPath.section, indexPath.row);
        
        NSString* tournamentName = [[self.tournaments objectAtIndex:indexPath.row] name];
        
        cell.tournamentNameLabel.text = tournamentName;
        
        return cell;
        
    } else {
        
        UITableViewCell* cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"Cell"];
        
        cell.textLabel.text = @"Редактировать команды";
        
        return cell;
        
    }
    
    return nil;
}

#pragma mark - NSFetchedResultsController

- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    // Edit the entity name as appropriate.
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"MTTournament" inManagedObjectContext:self.managedObjectContext];
    
    //    if (!entity) {
    //        return nil;
    //    }
    
    [fetchRequest setEntity:entity];
    
    // Set the batch size to a suitable number.
    //    [fetchRequest setFetchBatchSize:1];
    
    // Edit the sort key as appropriate.
//    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"number" ascending:YES];
    
//    [fetchRequest setSortDescriptors:@[sortDescriptor]];
    
//    NSPredicate* measurementsOfLastSessionPredicate = [NSPredicate predicateWithFormat:@"measurementSession.sessionID = %ld", self.currentSessionID];
    
//    [fetchRequest setPredicate:measurementsOfLastSessionPredicate];
    
    // Edit the section name key path and cache name if appropriate.
    // nil for section name key path means "no sections".
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    aFetchedResultsController.delegate = self;
    self.fetchedResultsController = aFetchedResultsController;
    
    NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _fetchedResultsController;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        default:
            return;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableView *tableView = self.tableView;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationTop];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
//            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            NSLog(@"UPDATE");
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView endUpdates];
}

#pragma mark - Segue

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"createTournament"]) {
        
        NSLog(@"CREATE");
        
    }
    
}


@end
