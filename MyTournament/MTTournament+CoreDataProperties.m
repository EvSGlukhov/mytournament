//
//  MTTournament+CoreDataProperties.m
//  MyTournament
//
//  Created by Евгений Глухов on 30.03.17.
//  Copyright © 2017 Evgeny Glukhov. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "MTTournament+CoreDataProperties.h"

@implementation MTTournament (CoreDataProperties)

@dynamic countOfGroups;
@dynamic countOfTeamsInGroup;
@dynamic countOfTeamsToPlayoff;
@dynamic finalInRoundRobin;
@dynamic halftimeDuration;
@dynamic name;
@dynamic participantsCount;
@dynamic participantsType;
@dynamic playoffGamesInFinal;
@dynamic playoffHomeAwayMatches;
@dynamic playoffMatchesSchema;
@dynamic pointsDistribution;
@dynamic roundsCount;
@dynamic toPlayoffPriorities;
@dynamic type;
@dynamic players;
@dynamic stages;
@dynamic statistics;

@end
