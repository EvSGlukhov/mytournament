//
//  MTStage+CoreDataProperties.h
//  MyTournament
//
//  Created by Евгений Глухов on 30.03.17.
//  Copyright © 2017 Evgeny Glukhov. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "MTStage.h"

NS_ASSUME_NONNULL_BEGIN

@interface MTStage (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *currentType;
@property (nullable, nonatomic, retain) NSString *globalType;
@property (nullable, nonatomic, retain) NSSet<MTGroup *> *groupTables;
@property (nullable, nonatomic, retain) NSSet<MTMatchWeek *> *matchweeks;
@property (nullable, nonatomic, retain) NSSet<MTGroup *> *playoffTables;
@property (nullable, nonatomic, retain) MTTournament *tournament;

@end

@interface MTStage (CoreDataGeneratedAccessors)

- (void)addGroupTablesObject:(MTGroup *)value;
- (void)removeGroupTablesObject:(MTGroup *)value;
- (void)addGroupTables:(NSSet<MTGroup *> *)values;
- (void)removeGroupTables:(NSSet<MTGroup *> *)values;

- (void)addMatchweeksObject:(MTMatchWeek *)value;
- (void)removeMatchweeksObject:(MTMatchWeek *)value;
- (void)addMatchweeks:(NSSet<MTMatchWeek *> *)values;
- (void)removeMatchweeks:(NSSet<MTMatchWeek *> *)values;

- (void)addPlayoffTablesObject:(MTGroup *)value;
- (void)removePlayoffTablesObject:(MTGroup *)value;
- (void)addPlayoffTables:(NSSet<MTGroup *> *)values;
- (void)removePlayoffTables:(NSSet<MTGroup *> *)values;

@end

NS_ASSUME_NONNULL_END
